// nome do pacote
package apiTest;

// Bibliotecas


import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;


// Classe
public class TesteUser { // inicio da classe
    // Atributos
    static String ct = "application/json"; // content type
    static String uriUser = "https://petstore.swagger.io/v2/user/";
    // Funções e Métodos
    // Funções de Apoio
    public static String lerArquivoJson(String arquivoJson) throws IOException {
        return new String(Files.readAllBytes(Paths.get(arquivoJson)));
    }

    // Funções de Teste
    @Test
    public void testarIncluirUser() throws IOException {
        // carregar os dados do json
        String jsonBody = lerArquivoJson("src/test/resources/json/user1.json");

    String userId = "1370535223";
    // realizar o teste
      given()                                              //Dado que
                .contentType(ct)          // o tipo do conteúdo
                .log().all()                                 // mostre tudo
                .body(jsonBody)                              // corpo da requisição
        .when()                                              // Quando
                .post("https://petstore.swagger.io/v2/user") // Endpoint / Onde
        .then()                                              // Então
                .log().all()                                 // mostre tudo ba volta
                .statusCode(200)                           // comunic. ida e volta ok
                .body("code", is(200))               // tag code é 200
                .body("type", is("unknown"))         // tag type é "unknown"
                .body("message", is(userId))              // message é o userID
        ;
    } //fim da post

    public void testarConsultarUser(){
        String username = "Romario";

        given()
                .contentType(ct)
                .log().all()
        .when()
                .
    }











} // fim da classe








